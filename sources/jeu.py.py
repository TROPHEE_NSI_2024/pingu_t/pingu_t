import pygame
from pygame.locals import *
import random
import math
import time

pygame.init()
fenetre = pygame.display.set_mode((1600, 900))

perso = pygame.image.load("perso.png").convert()

mur = pygame.image.load("mur.png").convert()

rien = pygame.image.load("p.png").convert()

TROU = pygame.image.load("trou.png").convert()

vic = pygame.image.load("mini_vic.png").convert()

blanc = pygame.image.load("blanc.png").convert()

fond = pygame.image.load("fond.png").convert()

vic_final = pygame.image.load("Victoire.png").convert()

debut = pygame.image.load("debut.png").convert()

gene = pygame.image.load("attente.png").convert()

dingz = pygame.image.load("fin_vrai.png").convert()


r0 = pygame.image.load("0.png").convert()
r1 = pygame.image.load("1.png").convert()
r2 = pygame.image.load("2.png").convert()
r3 = pygame.image.load("3.png").convert()
r4 = pygame.image.load("4.png").convert()
r5 = pygame.image.load("5.png").convert()
r6 = pygame.image.load("6.png").convert()
r7 = pygame.image.load("7.png").convert()
r8 = pygame.image.load("8.png").convert()
r9 = pygame.image.load("9.png").convert()

pr0 = pygame.image.load("p0.png").convert()
pr1 = pygame.image.load("p1.png").convert()
pr2 = pygame.image.load("p2.png").convert()
pr3 = pygame.image.load("p3.png").convert()
pr4 = pygame.image.load("p4.png").convert()
pr5 = pygame.image.load("p5.png").convert()
pr6 = pygame.image.load("p6.png").convert()
pr7 = pygame.image.load("p7.png").convert()
pr8 = pygame.image.load("p8.png").convert()
pr9 = pygame.image.load("p9.png").convert()

zr0 = pygame.image.load("0R.png").convert()
zr1 = pygame.image.load("1R.png").convert()
zr2 = pygame.image.load("2R.png").convert()
zr3 = pygame.image.load("3R.png").convert()
zr4 = pygame.image.load("4R.png").convert()
zr5 = pygame.image.load("5R.png").convert()
zr6 = pygame.image.load("6R.png").convert()
zr7 = pygame.image.load("7R.png").convert()
zr8 = pygame.image.load("8R.png").convert()
zr9 = pygame.image.load("9R.png").convert()

fenetre.blit(fond, (1200,0))
fenetre.blit(r0, (1350,777))


def plateau_vierge(m,n):
    """
    la fonction renvoie un plateau de jeu vierge
    """
    li = []
    for i in range(n+2) :
        li.append([])
        for k in range(m+2):
            li[i].append(0)
    for i in range(n+2):
        li[i][0] = '|'
        li[i][-1] = '|'
    for i in range(m+2):
        li[0][i] = '-'
        li[-1][i] = '-'
    li[0][0] = '+'
    li[-1][0] = '+'
    li[0][-1] = '+'
    li[-1][-1] = '+'
    return li


def affiche_pour_le_code(li) :
    """
    affiche le plateau pris en paramétre dans la console 
    """
    for e in li :
        t = ''
        for k in e :
            t += str(k) + ' '
        print(t)


def mur_hor(li,x,y,l):
    """
    permet de créer un mur horizontale sur le plateau (li) a la position x et y et de longueur l 
    """
    for i in range(l):
        if li[y][x+i] == 0 :
            li[y][x+i] = '-'
        else :
            break

        
def mur_ver(li,x,y,l):
    """
    permet de créer un mur vertical sur le plateau (li) a la position x et y et de longueur l 
    """
    for i in range(l):
        if li[y+i][x] == 0 :
            li[y+i][x] = '|'
        else :
            break
   
def joueur(li,x,y):
    x0,y0 = position_j(li)     
    li[y0][x0] = 0
    li[y][x] = '.'
    

def position_j(li):
    """
    renvoie la position du joueur sur le plateau ( li )
    """
    y = 0
    for e in li :
        x = 0
        for k in e :
            if k == '.' :
                break
            x += 1
        if '.' in e :
            break
        y +=1
    return (x,y)

def position_s(li):
    """
    renvoie la position de la sortie du niveau sur le plateau ( li )
    """
    y = 0
    for e in li :
        x = 0
        for k in e :
            if k == 'S' :
                break
            x += 1
        if 'S' in e :
            break
        y +=1
    return (x,y)

def haut(li):
    """
    permet de déplacer le personnage vers le haut
    """
    x,y = position_j(li)
    while li[y-1][x] == 0 or li[y-1][x] == 'S'  :
        joueur(li,x,y-1)
        if li[y-1][x] == 'S' :
            break
        y -= 1

def bas(li):
    """
    permet de déplacer le personnage vers le bas
    """
    x,y = position_j(li)
    while li[y+1][x] == 0 or li[y+1][x] == 'S' :
        joueur(li,x,y+1)
        if li[y+1][x] == 'S' :
            break
        y += 1
        
def droite(li):
    """
    permet de déplacer le personnage vers la droite
    """
    x,y = position_j(li)
    while li[y][x+1] == 0 or li[y][x+1] == 'S' :
        joueur(li,x+1,y)
        if li[y][x+1] == 'S' :
            break
        x += 1

def gauche(li):
    """
    permet de déplacer le personnage vers la gauche
    """
    x,y = position_j(li)
    while li[y][x-1] == 0 or li[y][x-1] == 'S' :
        joueur(li,x-1,y)
        if li[y][x-1] == 'S' :
            break
        x -= 1
        

def est_win(li):
    """
    renvoie True si le plateau (li) est gagner sinon False
    """
    for e in li :
        if 'S' in e :
            return False
    return True

def position_posible(li):
    """
    renvoie une liste de coordonéés où il n'y a ni de mur ni de joueur ni de sortie dessus
    """
    renc
    l = []
    y = 0
    for e in li :
        x = 0
        for k in e :
            if k == 0 :
                l.append((x,y))
            x += 1
        y +=1
    return l



def plateau_1():
    """
    renvoie le premier plateau du jeu 
    """
    l = plateau_vierge(38,28)
    l[1][1] = '.'
    l[28][30] = 'S'
    mur_hor(l,1,2,6)
    mur_hor(l,11,1,1)
    mur_hor(l,6,4,7)
    mur_hor(l,31,3,1)
    return l

def plateau_imp():
    """
    renvoie un plateau impossible utiliser pour des test
    """
    l = plateau_vierge(38,28)
    l[1][1] = '.'
    l[28][30] = 'S'
    mur_hor(l,1,2,6)
    mur_ver(l,11,1,3)
    mur_hor(l,1,4,11)
    mur_hor(l,31,3,1)
    return l

def affiche(plateau):
    """
    permet d'afficher le plateau sur l'écran du jeu
    """
    y = 0
    for e in plateau:
        x = 0
        for k in e:
            if k == '.':
                fenetre.blit(perso, (x,y))
            elif k == '|' or k == '-' or k == '+' :
                fenetre.blit(mur, (x,y))
            elif k == 0 :
                fenetre.blit(rien, (x,y))
            elif k == 'S' :
                fenetre.blit(TROU, (x,y))
            x+= 30
        y+= 30
        
def modif_score(s):
    """
    permet de modifié le score de 10 utiliser a chaque déplacement
    """
    s = int(s) + 10
    return str(s)

def modif_niv(s):
    """
    permet de modifié le niveau de 1 utiliser a chaque fin de niveau sert pour l'affichage du niveau
    """
    s = int(s) + 1
    if s < 10 :
        return '0' + str(s)
    return str(s)

def angle_d(a) :
    """
    permet de modifié l'angle de pingu si il se déplace a droite
    """
    if a  == 90 :
        return 0
    elif a == 180 :
        return -90
    elif a == 270 :
        return -180
    else :
        return 90

def angle_g(a) :
    """
    permet de modifié l'angle de pingu si il se déplace a gauche
    """
    if a  == 270 :
        return 0
    elif a == 0 :
        return 270
    elif a == 90 :
        return 180
    else :
        return 90
    
def angle_b(a) :
    """
    permet de modifié l'angle de pingu si il se déplace vers le bas 
    """
    if a  == 0 :
        return 0
    elif a == 90 :
        return -90
    elif a == 180 :
        return -180
    else :
        return -270

def angle_h(a) :
    """
    permet de modifié l'angle de pingu si il se déplace vers le haut
    """
    if a  == 180 :
        return 0
    elif a == 270 :
        return -90
    elif a == 0 :
        return 180
    else :
        return 90

def affiche_niv(s):
    """
    permet d'afficher le numéro du niveau actuelle 
    """
    chiffre_act = r0
    x = 1375
    for e in s :
        if int(e) == 0 :
            chiffre_act = r0
        elif int(e) == 1 :
            chiffre_act = r1
        elif int(e) == 2 :
            chiffre_act = r2
        elif int(e) == 3 :
            chiffre_act = r3
        elif int(e) == 4 :
            chiffre_act = r4
        elif int(e) == 5 :
            chiffre_act = r5
        elif int(e) == 6 :
            chiffre_act = r6
        elif int(e) == 7 :
            chiffre_act = r7
        elif int(e) == 8 :
            chiffre_act = r8
        elif int(e) == 9 :
            chiffre_act = r9
        fenetre.blit(chiffre_act, (x,557))
        x+=100

def affiche_score(s):
    """
    permet d'afficher le score actuelle du joueur
    """
    len_l = len(s)
    chiffre_act = r0

    if len_l < 3 :
        x = 1300
        for e in s :
            if int(e) == 0 :
                chiffre_act = r0
            elif int(e) == 1 :
                chiffre_act = r1
            elif int(e) == 2 :
                chiffre_act = r2
            elif int(e) == 3 :
                chiffre_act = r3
            elif int(e) == 4 :
                chiffre_act = r4
            elif int(e) == 5 :
                chiffre_act = r5
            elif int(e) == 6 :
                chiffre_act = r6
            elif int(e) == 7 :
                chiffre_act = r7
            elif int(e) == 8 :
                chiffre_act = r8
            elif int(e) == 9 :
                chiffre_act = r9
            fenetre.blit(chiffre_act, (x,777))
            x+=100
            
    elif len_l < 4 :
        fenetre.blit(blanc, (1300,777))
        x = 1230
        for e in s :
            if int(e) == 0 :
                chiffre_act = r0
            elif int(e) == 1 :
                chiffre_act = r1
            elif int(e) == 2 :
                chiffre_act = r2
            elif int(e) == 3 :
                chiffre_act = r3
            elif int(e) == 4 :
                chiffre_act = r4
            elif int(e) == 5 :
                chiffre_act = r5
            elif int(e) == 6 :
                chiffre_act = r6
            elif int(e) == 7 :
                chiffre_act = r7
            elif int(e) == 8 :
                chiffre_act = r8
            elif int(e) == 9 :
                chiffre_act = r9
            fenetre.blit(chiffre_act, (x,777))
            x+=120
            
def affiche_score_bot(s) :
    """
    permet d'afficher le score du bot sur le mini-écran de fin entre chaque niveau si le score du joueur est inférieur a celui du bot
    """
    len_l = len(s)
    chiffre_act = r0
    if len_l < 3 :
        x = 550
        for e in s :
            if int(e) == 0 :
                chiffre_act = r0
            elif int(e) == 1 :
                chiffre_act = r1
            elif int(e) == 2 :
                chiffre_act = r2
            elif int(e) == 3 :
                chiffre_act = r3
            elif int(e) == 4 :
                chiffre_act = r4
            elif int(e) == 5 :
                chiffre_act = r5
            elif int(e) == 6 :
                chiffre_act = r6
            elif int(e) == 7 :
                chiffre_act = r7
            elif int(e) == 8 :
                chiffre_act = r8
            elif int(e) == 9 :
                chiffre_act = r9
            fenetre.blit(chiffre_act, (x,400))
            x+=100
            
    elif len_l < 4 :
        x = 430
        for e in s :
            if int(e) == 0 :
                chiffre_act = r0
            elif int(e) == 1 :
                chiffre_act = r1
            elif int(e) == 2 :
                chiffre_act = r2
            elif int(e) == 3 :
                chiffre_act = r3
            elif int(e) == 4 :
                chiffre_act = r4
            elif int(e) == 5 :
                chiffre_act = r5
            elif int(e) == 6 :
                chiffre_act = r6
            elif int(e) == 7 :
                chiffre_act = r7
            elif int(e) == 8 :
                chiffre_act = r8
            elif int(e) == 9 :
                chiffre_act = r9
            fenetre.blit(chiffre_act, (x,400))
            x+=120


def copie_plateau(p):
    """
    renvoie un nouveau plateau identique a celui entrée en paramétre
    """
    l = plateau_vierge(38,28)
    for y in range(30) :
        for x in range(40):
            l[y][x] = p[y][x]
    return l
    
    
def coup_possible(p,cp):
    """
    liste des coups possible pour le personnage selon son dernier coups pour ne pas pouvoire retourner en arriére utiliser pour le bot
    """
    solutions = []
    pb = position_j(p)
    p1 = copie_plateau(p)
    p2 = copie_plateau(p)
    p3 = copie_plateau(p)
    p4 = copie_plateau(p)
    haut(p1)
    bas(p2)
    gauche(p3)
    droite(p4)
    if position_j(p1) != pb and cp != 'bas' :
        solutions.append('haut')
    if position_j(p2) != pb and cp != 'haut' :
        solutions.append('bas')
    if position_j(p3) != pb and cp != 'droite' :
        solutions.append('gauche')
    if position_j(p4) != pb and cp != 'gauche' :
        solutions.append('droite')
    return solutions

def deplacement(e,p):
    """
    deplace le personnage selon un texte
    """
    if e == 'haut' :
        haut(p)
    elif e == 'gauche' :
        gauche(p)
    elif e == 'bas' :
        bas(p)
    elif e == 'droite' :
        droite(p)
         
def afficher_chargement(num):
    """
    affiche le chargement d'un plateau aléatoire sert a rien concretement mais on trouvé ça beau
    """
    n = str(num)
    chiffre_act = zr0
    if len(n) == 1 :
        if int(n) == 1 :
            chiffre_act = zr1
        elif int(n) == 2 :
            chiffre_act = zr2
        elif int(n) == 3 :
            chiffre_act = zr3
        elif int(n) == 4 :
            chiffre_act = zr4
        elif int(n) == 5 :
            chiffre_act = zr5
        elif int(n) == 6 :
            chiffre_act = zr6
        elif int(n) == 7 :
            chiffre_act = zr7
        elif int(n) == 8 :
            chiffre_act = zr8
        elif int(n) == 9 :
            chiffre_act = zr9
        fenetre.blit(chiffre_act, (800,500))
        fenetre.blit(zr0, (700,500))
            
    else :
        if int(n[1]) == 1 :
            chiffre_act = zr1
        elif int(n[1]) == 2 :
            chiffre_act = zr2
        elif int(n[1]) == 3 :
            chiffre_act = zr3
        elif int(n[1]) == 4 :
            chiffre_act = zr4
        elif int(n[1]) == 5 :
            chiffre_act = zr5
        elif int(n[1]) == 6 :
            chiffre_act = zr6
        elif int(n[1]) == 7 :
            chiffre_act = zr7
        elif int(n[1]) == 8 :
            chiffre_act = zr8
        elif int(n[1]) == 9 :
            chiffre_act = zr9
        fenetre.blit(chiffre_act, (800,500))
        fenetre.blit(zr1, (700,500))
        

def est_possible(p,cp,nbtf,l,z,score,ls):
    """
    algorithme qui va faire toute les possibilite de déplacement sur un nombre de coups limites ici 15 
    """
    if est_win(p) :
        z = True
        ls.append(score)
    if z == True :
        return True
    if not est_win(p) and nbtf <= 15 :
        afficher_chargement(nbtf)
        pygame.display.flip() 
        nbtf += 1
        score += 10
        a = coup_possible(p,cp)
        if len(a) == 1 :
            p1 = copie_plateau(p)
            deplacement(a[0],p1)
            a1 = est_possible(p1,a[0],nbtf,l,z,score,ls)
            l.append(a1)
            
        elif len(a) == 2 :
            p1 = copie_plateau(p)
            p2 = copie_plateau(p)
            deplacement(a[0],p1)
            deplacement(a[1],p2)
            a1 = est_possible(p1,a[0],nbtf,l,z,score,ls)
            b1 = est_possible(p2,a[1],nbtf,l,z,score,ls)
            l.append(a1)
            l.append(b1)
            
            
        elif len(a) == 3 :
            p1 = copie_plateau(p)
            p2 = copie_plateau(p)
            p3 = copie_plateau(p)
            deplacement(a[0],p1)
            deplacement(a[1],p2)
            deplacement(a[2],p3)
            a1 = est_possible(p1,a[0],nbtf,l,z,score,ls)
            b1 = est_possible(p2,a[1],nbtf,l,z,score,ls)
            c1 = est_possible(p3,a[2],nbtf,l,z,score,ls)
            l.append(a1)
            l.append(b1)
            l.append(c1)
            
        elif len(a) == 4 :
            p1 = copie_plateau(p)
            p2 = copie_plateau(p)
            p3 = copie_plateau(p)
            p4 = copie_plateau(p)
            deplacement(a[0],p1)
            deplacement(a[1],p2)
            deplacement(a[2],p3)
            deplacement(a[3],p4)
            a1 = est_possible(p1,a[0],nbtf,l,z,score,ls)
            b1 = est_possible(p2,a[1],nbtf,l,z,score,ls)
            c1 = est_possible(p3,a[2],nbtf,l,z,score,ls)
            d1 = est_possible(p4,a[3],nbtf,l,z,score,ls)
            l.append(a1)
            l.append(b1)
            l.append(c1)
            l.append(d1)
    if nbtf > 15 :
        if l == [False] :
            l.append(False)
        else :
            if True in l :
                return l
            if None in l :
                for e in range(len(l)) :
                    if l[e] == None :
                        l[e] = False
            if not True in l :
                return l
            
def verif(p):
    """
    verifie si un plateau est possible ( si c'est le cas il renvera aussi le valeur de score minimum )
    """
    cp = 'd'
    nbtf = 0
    l = [False]
    z = False
    ls = []
    alors = est_possible(p,cp,nbtf,l,z,0,ls)
    for e in l :
        if e == True :
            return True , min(ls)
        if e == False :
            None
        if e == None :
            None
        if type(e) == list  :
            if True in e :
                return True , min(ls)
            return False , 0
     
    
    
    
def plateau_alea(m,n):
    """
    renvoie un plateau généré de maniére aléatoire
    """
    l = plateau_vierge(m,n)
    f = random.randint(min(m,n)//4,max(m,n)//4)
    if f < m // 2 :
        f = f * 4
    for i in range(f) :
        z = random.choice(position_posible(l))
        x,y = z
        mur_hor(l,x,y,random.randint(1,n//4))
        
        z0 = random.choice(position_posible(l))
        x0,y0 = z0
        mur_ver(l,x0,y0,random.randint(1,m//4))
        
    x1,y1 = random.choice(position_posible(l))
    l[y1][x1] = 'S'
    x2,y2 = random.choice(position_posible(l))
    l[y2][x2] = '.'
    return l

plateau = plateau_1()
plateau_rei = copie_plateau(plateau)
score = '0'
niv = '01'
angle = 0
sco = str(verif(plateau)[1])
affiche_score_bot(sco)
affiche_niv(niv)
affiche(plateau)
pygame.display.flip()

ds = True
ds_2 = False




continuer = True
while continuer:
    for event in pygame.event.get():
        if ds == True :
            fenetre.blit(debut,(0,0))
            pygame.display.flip()
            if event.type == KEYDOWN :
                if event.key == K_SPACE :
                    ds = False
                    fenetre.blit(fond, (1200,0))
                    affiche(plateau)
                    affiche_niv(niv)
                    affiche_score(score)
                    pygame.display.flip()
                    
        if event.type == QUIT:
            continuer = False
            
        if event.type == KEYDOWN and  est_win(plateau) :
            if event.key == K_SPACE :
                ds_2 = True
                fenetre.blit(gene, (0,0))
                pygame.display.flip()
                niv = modif_niv(niv)
                ver = False
                while ver != True :
                    plateau = plateau_alea(38,28) 
                    ver,sco = verif(plateau)
                plateau_rei = copie_plateau(plateau)
                score = '0'
                affiche_score(score)
                fenetre.blit(fond, (1200,0))
                affiche_niv(niv)
                bs = str(sco)
                affiche(plateau)
                pygame.display.flip()
                ds_2 = False
                
        if event.type == KEYDOWN and not  est_win(plateau) and ds == False and ds_2 == False :
            if event.key == K_z or event.key == K_UP :
                perso = pygame.transform.rotate(perso,angle_h(angle))
                angle = angle + angle_h(angle)
                haut(plateau)
                affiche(plateau)
                score = modif_score(score)
                affiche_score(score)
                pygame.display.flip()
            if event.key == K_q or event.key == K_LEFT:
                perso = pygame.transform.rotate(perso,angle_g(angle))
                angle = angle + angle_g(angle)
                gauche(plateau)
                affiche(plateau)
                score = modif_score(score)
                affiche_score(score)
                pygame.display.flip()
            if event.key == K_s or event.key == K_DOWN:
                perso = pygame.transform.rotate(perso,angle_b(angle))
                angle = angle + angle_b(angle)
                bas(plateau)
                affiche(plateau)
                score = modif_score(score)
                affiche_score(score)
                pygame.display.flip()
            if event.key == K_d or event.key == K_RIGHT:
                perso = pygame.transform.rotate(perso,angle_d(angle))
                angle = angle + angle_d(angle)
                droite(plateau)
                affiche(plateau)
                score = modif_score(score)
                affiche_score(score)
                pygame.display.flip()
            if event.key == K_r :    
                plateau = copie_plateau(plateau_rei)
                plateau_rei = copie_plateau(plateau)
                affiche_score(score)
                affiche(plateau)
                pygame.display.flip()
        if  est_win(plateau):
            if int(score) > int(sco) :
                fenetre.blit(vic, (0,0))
                affiche_score_bot(str(sco))
                pygame.display.flip()
            else :
                fenetre.blit(vic_final, (0,0))
                pygame.display.flip()
        if est_win(plateau) and niv == '25' :
            ds_2 = True
            fenetre.blit(dingz, (0,0))
            pygame.display.flip()
            time.sleep(10)
            continuer = False
            
                
pygame.display.quit() 